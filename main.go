package main

import (
	"bytes"
	"context"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net"
	"net/url"
	"os"
	"sort"
	"strconv"
	"strings"
	"text/tabwriter"

	"github.com/spf13/cobra"
	"github.com/tendermint/tendermint/rpc/client/http"
	"github.com/tendermint/tendermint/rpc/coretypes"
	"gitlab.com/accumulatenetwork/accumulate/config"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
)

var cmd = &cobra.Command{
	Use:   "network-status <DNN>",
	Short: "Walk the network and get the status of the nodes",
	Args:  cobra.MinimumNArgs(1),
	Run:   run,
}

var flag = struct {
	Debug bool
	Json  bool
}{}

func main() {
	cmd.Flags().BoolVarP(&flag.Debug, "debug", "d", false, "Debug")
	cmd.Flags().BoolVarP(&flag.Json, "json", "j", false, "Output JSON")
	_ = cmd.Execute()
}

type NodeData struct {
	Hostname    string                  `json:"hostname"`
	BasePort    uint64                  `json:"basePort"`
	ValidatorID [20]byte                `json:"validatorID"`
	DnNodeID    [20]byte                `json:"dnNodeID"`
	DnnStatus   *coretypes.ResultStatus `json:"dnnStatus"`
	BvnNodeID   [20]byte                `json:"bvnNodeID"`
	BvnnStatus  *coretypes.ResultStatus `json:"bvnnStatus"`
	Info        *protocol.ValidatorInfo `json:"info"`
}

func run(_ *cobra.Command, args []string) {
	var nodes []*NodeData
	byNodeId := map[[20]byte]*NodeData{}

	hostname, port := parseAddr(args[0])
	addr := fmt.Sprintf("http://%s:%d", hostname, port+uint64(config.PortOffsetAccumulateApi))
	if flag.Debug {
		fmt.Printf("Accumulate describe %s\n", addr)
	}
	acc, err := client.New(addr)
	checkf(err, "new DNN Acc client")

	describe, err := acc.Describe(context.Background())
	checkf(err, "DNN Acc describe")

	valInfo := map[[20]byte]*protocol.ValidatorInfo{}
	for _, val := range describe.Values.Network.Validators {
		id := val.PublicKeyHash[:20]
		valInfo[*(*[20]byte)(id)] = val
	}

	addrs := args
	for len(addrs) > 0 {
		var next []string
		for _, addr := range addrs {
			hostname, port := parseAddr(addr)

			addr := fmt.Sprintf("http://%s:%d", hostname, port+uint64(config.PortOffsetTendermintRpc))
			if flag.Debug {
				fmt.Printf("Tendermint status %s\n", addr)
			}
			tm, err := http.New(addr)
			checkf(err, "new DNN TM client")

			status, err := tm.Status(context.Background())
			checkf(err, "DNN TM status")

			nodeId, err := hex.DecodeString(string(status.NodeInfo.NodeID))
			checkf(err, "parse node ID")

			valId, err := hex.DecodeString(status.ValidatorInfo.Address.String())
			checkf(err, "parse validator ID")

			_, ok := byNodeId[*(*[20]byte)(nodeId)]
			if ok {
				continue
			}

			n := new(NodeData)
			n.Hostname = hostname
			n.BasePort = port
			n.ValidatorID = *(*[20]byte)(valId)
			n.Info = valInfo[n.ValidatorID]
			n.DnNodeID = *(*[20]byte)(nodeId)
			n.DnnStatus = status
			delete(valInfo, n.ValidatorID)

			nodes = append(nodes, n)
			byNodeId[n.DnNodeID] = n

			if flag.Debug {
				fmt.Printf("Tendermint net-info %s\n", addr)
			}
			netInfo, err := tm.NetInfo(context.Background())
			checkf(err, "DNN TM net info")

			for _, peer := range netInfo.Peers {
				id, err := hex.DecodeString(string(peer.ID))
				checkf(err, "parse peer ID")
				if byNodeId[*(*[20]byte)(id)] == nil {
					next = append(next, peer.URL)
				}
			}

			addr = fmt.Sprintf("http://%s:%d", hostname, port+uint64(config.PortOffsetTendermintRpc+config.PortOffsetBlockValidator))
			if flag.Debug {
				fmt.Printf("Tendermint status %s\n", addr)
			}
			tm, err = http.New(addr)
			checkf(err, "new BVNN TM client")

			status, err = tm.Status(context.Background())
			checkf(err, "BVNN TM status")

			nodeId, err = hex.DecodeString(string(status.NodeInfo.NodeID))
			checkf(err, "parse node ID")

			n.BvnNodeID = *(*[20]byte)(nodeId)
			n.BvnnStatus = status
			byNodeId[n.BvnNodeID] = n
		}
		addrs = next
	}

	for id, val := range valInfo {
		n := new(NodeData)
		n.ValidatorID = id
		n.Info = val
		nodes = append(nodes, n)
	}

	sort.Slice(nodes, func(i, j int) bool {
		return bytes.Compare(nodes[i].ValidatorID[:], nodes[j].ValidatorID[:]) < 0
	})

	if flag.Json {
		b, err := json.MarshalIndent(nodes, "", "    ")
		checkf(err, "encode")
		println(string(b))
		return
	}

	tw := tabwriter.NewWriter(os.Stdout, 2, 4, 1, ' ', 0)
	defer tw.Flush()

	fmt.Fprintf(tw, "Validator\tNode\tAddress\tActive\n")
	for _, n := range nodes {
		var addr string
		if n.Hostname != "" {
			addr = fmt.Sprintf("%s:%d", n.Hostname, n.BasePort)
		}
		var active []string
		if n.Info != nil {
			for _, part := range n.Info.Partitions {
				active = append(active, part.ID)
			}
		}

		fmt.Fprintf(tw, "%x\t%x\t%s\t%s\n", sliceOrNil(n.ValidatorID), sliceOrNil(n.DnNodeID), addr, strings.Join(active, ","))
	}
}

func parseAddr(s string) (string, uint64) {
	u, err := url.Parse(s)
	checkf(err, "parse url %q", s)

	port, err := strconv.ParseUint(u.Port(), 10, 64)
	checkf(err, "parse port of %q", s)
	port -= uint64(config.PortOffsetTendermintP2P + config.PortOffsetDirectory)

	hostname := u.Hostname()
	if net.ParseIP(hostname) != nil {
		return hostname, port
	}

	ip, err := net.LookupIP(hostname)
	if err != nil {
		return hostname, port
	}

	hostname = ip[0].String()
	return hostname, port
}

func fatalf(format string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, "Error: "+format+"\n", args...)
	os.Exit(1)
}

func checkf(err error, format string, otherArgs ...interface{}) {
	if err != nil {
		fatalf(format+": %v", append(otherArgs, err)...)
	}
}

func sliceOrNil(id [20]byte) []byte {
	if id == ([20]byte{}) {
		return nil
	}
	return id[:]
}
